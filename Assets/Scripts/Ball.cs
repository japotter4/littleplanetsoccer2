﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    public float gravity;
    public float minForceOnHit;
    public float forceModifier;
    public float maxVelocity;

    private Rigidbody _rigidBody;
    private GameObject _planet;

	void Start () {
        _rigidBody = GetComponent<Rigidbody>();
        _planet = GameObject.Find("Planet");
	}
	
	void FixedUpdate () {
        _rigidBody.AddForce((_planet.transform.position - transform.position).normalized * gravity, ForceMode.VelocityChange);

        if (_rigidBody.velocity.magnitude > maxVelocity) {
            _rigidBody.velocity = _rigidBody.velocity.normalized * maxVelocity;
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Player")) {
            Vector3 contactPoint = collision.contacts[0].point;

            float force = collision.gameObject.GetComponent<Rigidbody>().velocity.magnitude * forceModifier;

            if (force < minForceOnHit) {
                force = minForceOnHit;
            }

            _rigidBody.AddForce((transform.position - contactPoint).normalized * force, ForceMode.Impulse);
        }
    }
}
