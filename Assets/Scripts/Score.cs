﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour {

    public int PlayerID;
    public static int Player1score = 0;
    public static int Player2score = 0;

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ball")) {
            if (PlayerID == 1) { Player2score++; }
            if (PlayerID == 1) {
                Debug.LogFormat("Player2 Scored! {1} to {0}", Player2score, Player1score);
            }
                if (PlayerID == 2) { Player1score ++; }
            if (PlayerID == 2) {
                Debug.LogFormat("Player1 Scored! {1} to {0}", Player2score, Player1score);
            }
        }
            
    }
}
