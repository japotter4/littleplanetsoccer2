﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loadgame : MonoBehaviour {

    public void Loadlevel() 
        {SceneManager.LoadScene("SmallWorld"); }
    public void Loadtitle() 
        { SceneManager.LoadScene("UI"); }
    public void Loadhowto()
        { SceneManager.LoadScene("Controller Scene"); }
}
