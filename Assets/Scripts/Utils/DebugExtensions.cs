﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DebugX
{
    public static void LogAll(params object[] args) {
        string format = "";

        for(int i=0; i<args.Length; i++) {
            format += "{" + i + "} ";
        }

        Debug.LogFormat(format, args);
    }
}