﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformUtils {

    public static Transform FindChildNested(Transform t, string childName) {
        if (t.name == childName) {
            return t;
        } else if (t.childCount == 0) {
            return null;
        } else {

            Transform child = null;

            foreach(Transform c in t) {
                Transform foundChild = FindChildNested(c, childName);

                if (foundChild != null) {
                    child = foundChild;
                }
            }

            return child;
        }
    }
}
