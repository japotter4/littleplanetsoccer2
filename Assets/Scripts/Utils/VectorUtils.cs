﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorUtils {

    public static float AngleDir(Vector2 a, Vector2 b) {
        return -a.x * b.y + a.y * b.x;
    }

    // Returns true if a is left of b
    public static bool IsLeft(Vector2 a, Vector2 b) {
        return AngleDir(b, a) <= 0;
    }

    // Returns true if a is right of b
    public static bool IsRight(Vector2 a, Vector2 b) {
        return AngleDir(b, a) >= 0;
    }
}
