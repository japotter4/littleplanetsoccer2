﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerInput : MonoBehaviour
{
    protected Player _player;
    protected Rewired.Player _playerInput;

    public void SetPlayer(Player player) {
        _player = player;
        _playerInput = player.rewiredPlayer;
    }
}
