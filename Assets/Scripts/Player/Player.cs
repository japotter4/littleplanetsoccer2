﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player {

    public GameObject playerGameObject;
    public PlayerInput PlayerInput;
    public Rewired.Player rewiredPlayer;

    public int PlayerId { get; private set; }

    private GameObject _camera;

    public Player(int playerId) {
        PlayerId = playerId;
    }

    public void Init() {
        GameObject playerPrefab = Resources.Load("Player") as GameObject;
        playerGameObject = GameObject.Instantiate(playerPrefab);

        PlayerDriverInput input = playerGameObject.AddComponent<PlayerDriverInput>();
        input.SetPlayer(this);

        GameObject cameraPrefab = Resources.Load("Camera") as GameObject;

        _camera = GameObject.Instantiate(cameraPrefab);
        _camera.GetComponent<CameraFollow>().Init(playerGameObject);
    }

    public void UpdateCameras(int numPlayers) {
        Camera camera = _camera.GetComponent<Camera>();

        switch(numPlayers) {
            case 1:

                switch (PlayerId) {
                    case 0:
                        camera.rect = new Rect(0, 0, 1, 1);
                        break;
                }

                break;

            case 2:

                switch (PlayerId) {
                    case 0:
                        camera.rect = new Rect(0, 0, 0.5f, 1);
                        break;
                    case 1:
                        camera.rect = new Rect(0.5f, 0, 0.5f, 1);
                        break;
                }

                break;

            case 3:

                switch (PlayerId) {
                    case 0:
                        camera.rect = new Rect(0, 0, 0.5f, 0.5f);
                        break;
                    case 1:
                        camera.rect = new Rect(0.5f, 0, 0.5f, 0.5f);
                        break;
                    case 2:
                        camera.rect = new Rect(0, 0, 1, 0.5f);
                        break;
                }

                break;
        }
    }
}
