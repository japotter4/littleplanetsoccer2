﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class PlayerDriverInput : PlayerInput {

    private Car _car;

    private void Start() {
        _car = GetComponent<Car>();

        _car.Init(_player.PlayerId);
        _car.Reset();
    }

    private void Update() {
        if (_playerInput.GetButtonDown("Boost")) {
            _car.StartBoost();
        } else if (_playerInput.GetButtonUp("Boost")) {
            _car.StopBoost();
        }
    }

    private void FixedUpdate() {

        bool jump = _playerInput.GetButtonDown("Jump");
        bool onGround = _car.OnGround();

        if (onGround && jump) {
            _car.Jump();
        }

        if (onGround) {
            // pass the input to the car!
            float turning = _playerInput.GetAxis("HorizontalInput");
            float accel = _playerInput.GetAxis("Drive");
            bool handBrake = _playerInput.GetButton("HandBrake");

            _car.Move(turning, accel, handBrake);

            //Debug.Log("On ground");

        } else {
            //Debug.Log("Off ground");
            float yRotation = _playerInput.GetAxis("HorizontalInput");
            float zRotation = 0;
            float xRotation = _playerInput.GetAxis("VerticalInput");

            if (_playerInput.GetButton("yToZ")) {
                zRotation = -yRotation;
                yRotation = 0;
            }

            _car.Rotate(xRotation, yRotation, zRotation);
        }

        if (_playerInput.GetButtonLongPressDown("Reset")) {
            _car.Reset();
        }
    }
}
