﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Car : MonoBehaviour
{
    public float turnForce;
    public float handBrakeTurnForce;
    public float airRotationSpeedX;
    public float airRotationSpeedY;
    public float airRotationSpeedZ;
    public float maxAngularVelocityAir;
    public float jumpForce;
    public float boostSpeed;
    public float moveSpeed;
    public float groundedGravity;
    public float airGravity;
    public GameObject frontOfCar;
    public float maxVelocity;
    public float boostMaxVelocity;

    private GameObject _planet;
    private Rigidbody _rigidBody;
    private List<GameObject> _wheels = new List<GameObject>();
    private int _playerSide;
    private bool _isBoosting = false;

    private void Awake() {
        _rigidBody = GetComponent<Rigidbody>();
        _planet = GameObject.Find("Planet");

        foreach(Transform t in transform.FindChild("Wheels").transform) {
            _wheels.Add(t.gameObject);
        }
    }

    private void FixedUpdate() {
        bool onGround = OnGround();
        if (onGround) {
            _rigidBody.AddForce((_planet.transform.position - transform.position).normalized * groundedGravity, ForceMode.VelocityChange);
        } else {
            _rigidBody.AddForce((_planet.transform.position - transform.position).normalized * airGravity, ForceMode.VelocityChange);
        }

        float currentMaxVelocity = maxVelocity;

        if (_isBoosting) {
            _rigidBody.AddForce(transform.forward * boostSpeed);
            currentMaxVelocity = boostMaxVelocity;
        }

        if (_rigidBody.velocity.magnitude > currentMaxVelocity) {
            _rigidBody.velocity = _rigidBody.velocity.normalized * currentMaxVelocity;
        }

        if (!onGround && _rigidBody.angularVelocity.magnitude > maxAngularVelocityAir) {
            _rigidBody.angularVelocity = _rigidBody.angularVelocity.normalized * maxAngularVelocityAir;
        }
    }

    public void Init(int playerSide) {
        _playerSide = playerSide;
    }

    public void Jump() {
        _rigidBody.AddForce(transform.up * jumpForce);
    }

    public void Rotate(float x, float y, float z) {
        DebugX.LogAll(x, y, z);

        float threshold = 0.2f;

        if (Mathf.Abs(x) < threshold) {
            x = 0;
        } else if(Mathf.Abs(y) < threshold) {
            y = 0;
        } else if (Mathf.Abs(z) < threshold) {
            z = 0;
        }



            _rigidBody.AddRelativeTorque(x * airRotationSpeedX, y * airRotationSpeedY, z * airRotationSpeedZ);
    }

    public void StartBoost() {
        _isBoosting = true;
    }

    public void StopBoost() {
        _isBoosting = false;
    }

    //public void Boost() {
    //    _rigidBody.AddForce(transform.forward * boostSpeed);
    //}

    public void Move(float horizontal, float acceleration, bool handBrake) {

        float force = turnForce;

        if (handBrake) {
            force = handBrakeTurnForce;
        }

        if (Mathf.Abs(acceleration) < 0.3f) {
            horizontal = 0;
        }

        if (acceleration < 0) {
            horizontal *= -1;
        }

        if (horizontal != 0) {
            _rigidBody.AddForceAtPosition(gameObject.transform.right * horizontal * force, frontOfCar.transform.position);
        }

        _rigidBody.AddForce(acceleration * transform.forward * moveSpeed);
    }

    public bool OnGround() {

        bool onGround = false;

        foreach(GameObject wheel in _wheels) {
            RaycastHit hit = GetGroundHit(wheel);

            float maxDistanceOffGround = 0.3f + .35f;

            if ((wheel.transform.position - hit.point).magnitude < maxDistanceOffGround)
                onGround = true;
        }

        return onGround;
    }

    public RaycastHit GetGroundHit(GameObject gameObject) {
        RaycastHit hit;
        Physics.Raycast(gameObject.transform.position, -gameObject.transform.up, out hit, LayerMask.GetMask("Planet"));

        return hit;
    }

    public void Reset() {
        GameObject startingPosition = GameObject.Find("PlayerStarting" + _playerSide);

        gameObject.transform.position = startingPosition.transform.position;
        gameObject.transform.rotation = startingPosition.transform.rotation;
    }

    //// horizontal = -1, tire counter clockwise rotate, moves backwards
    //// horizontal = 1, tire clockwise rotate, moves forwards
    //public void Drive(float horizontal) {

    //    // Multiple by negative one because of default rotation directions
    //    horizontal *= -1;

    //    foreach (WheelJoint2D wheel in _wheels) {
    //        JointMotor2D motor = wheel.motor;

    //        motor.motorSpeed = horizontal * speed;

    //        wheel.motor = motor;
    //    }
    //}

    //public void Rotate(float direction) {
    //    //_rigidBody.AddTorque(-direction * rotationSpeed);
    //}

    //public void Reset() {
    //    Drive(0);
    //}
}
