﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class gravity : MonoBehaviour {

    public List<GameObject> objects;
    public GameObject planet;
    public GameObject ball;
    public float gravitationalPull;
    public float ballGravity;

    private void Awake() {
        GetAllObjects();
    }

    void FixedUpdate() {
       foreach (GameObject o in objects) {
           Rigidbody rigidbody = o.GetComponent<Rigidbody>();
            if (rigidbody && o != planet) {
                if (o == ball) {
                    rigidbody.AddForce((planet.transform.position - o.transform.position).normalized * ballGravity, ForceMode.VelocityChange);
                } else {
                    rigidbody.AddForce((planet.transform.position - o.transform.position).normalized * gravitationalPull, ForceMode.VelocityChange);
                }
            }
       }
    }

    public void GetAllObjects() {
        objects = UnityEngine.Object.FindObjectsOfType<GameObject>().ToList();
    }
}
     
     