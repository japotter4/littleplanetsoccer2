﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public float distanceBehind;
    public float distanceHeight;
    public float snapSpeed;

    private GameObject _planet;
    private GameObject _playerGameObject;
    private GameObject _ball;

    void Awake() {
        _ball = GameObject.Find("Ball");
        _planet = GameObject.Find("Planet");
    }

    public void Init(GameObject player) {
        _playerGameObject = player;
    }

    // Should work with player positions that are several frames old, that way player won't be able to move the camera in one direction
    // When at the edges
    void FixedUpdate () {
        Vector3 ballToPlayer = (_playerGameObject.transform.position - _ball.transform.position).normalized ;
        Vector3 playerToBall = (_ball.transform.position - _playerGameObject.transform.position).normalized ;
        Vector3 planetToPlayer = (_playerGameObject.transform.position - _planet.transform.position).normalized;
        Vector3 playerTangentToPlanet = Vector3.ProjectOnPlane(playerToBall, planetToPlayer).normalized;

        Vector3 behindPlayerTangentToPlanet = -playerTangentToPlanet * distanceBehind;

        Vector3 bEndPoint = _playerGameObject.transform.position + behindPlayerTangentToPlanet;
        Vector3 hEndPoint = bEndPoint + (planetToPlayer * distanceHeight);

        transform.position = hEndPoint;
        //transform.LookAt(_ball.transform.position, _playerGameObject.transform.up);

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(_ball.transform.position - transform.position, planetToPlayer), Time.deltaTime * snapSpeed);
	}
}
