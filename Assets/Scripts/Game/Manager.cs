﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class Manager : MonoBehaviour {

    public static Manager instance;

    public List<Player> Players { get; private set; }

    public int maxPlayers = 4;

    private List<PlayerMap> playerMap; // Maps Rewired Player ids to game player ids
    private int gamePlayerIdCounter = 0;

    void Awake() {
        instance = this;

        Players = new List<Player>();
        playerMap = new List<PlayerMap>();
    }

    void Update() {
        for (int i = 0; i < ReInput.players.playerCount; i++) {
            if (ReInput.players.GetPlayer(i).GetButtonDown("Join")) {
                AssignNextPlayer(i);
            }
        }
    }

    void AssignNextPlayer(int rewiredPlayerId) {
        if (playerMap.Count >= maxPlayers) {
            Debug.LogError("Max player limit already reached!");
            return;
        }

        int gamePlayerId = GetNextGamePlayerId();

        // Add the Rewired Player as the next open game player slot
        playerMap.Add(new PlayerMap(rewiredPlayerId, gamePlayerId));

        Rewired.Player rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerId);

        Player player = new Player(rewiredPlayerId);
        player.rewiredPlayer = rewiredPlayer;

        player.Init();

        Players.Add(player);

        // Disable the Assignment map category in Player so no more JoinGame Actions return
        rewiredPlayer.controllers.maps.SetMapsEnabled(false, "Guest");

        // Enable UI control for this Player now that he has joined
        rewiredPlayer.controllers.maps.SetMapsEnabled(true, "Default");

        Debug.Log("Added Rewired Player id " + rewiredPlayerId + " to game player " + gamePlayerId);

        foreach (Player p in Players) {
            p.UpdateCameras(Players.Count);
        }
    }

    private int GetNextGamePlayerId() {
        return gamePlayerIdCounter++;
    }

    // This class is used to map the Rewired Player Id to your game player id
    private class PlayerMap
    {
        public int rewiredPlayerId;
        public int gamePlayerId;

        public PlayerMap(int rewiredPlayerId, int gamePlayerId) {
            this.rewiredPlayerId = rewiredPlayerId;
            this.gamePlayerId = gamePlayerId;
        }
    }
}
